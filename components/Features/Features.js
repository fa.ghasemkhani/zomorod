// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { GiFullMotorcycleHelmet } from "react-icons/gi";
import { FaRegMoneyBillAlt } from "react-icons/fa";
import { AiOutlineUndo } from "react-icons/ai";
import { features } from "./style";

const Features = () => {
  return (
    <div css={features}>
      <span className="col">
        <GiFullMotorcycleHelmet className="featureIcon" /> <p className="text">Express Delivery</p>
      </span>
      <span className="col">
        <FaRegMoneyBillAlt className="featureIcon" /> <p className="text">Cash on Delivery</p>
      </span>
      <span className="col">
        <AiOutlineUndo className="featureIcon" /> <p className="text">Free Return</p>
      </span>
    </div>
  );
};

export default Features;
