import { css } from "@emotion/core";

export const features = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px 35px;

  .col {
    text-align: center;

    .featureIcon {
      color: #000;
      font-size: 40px;
    }

    .text {
      font-size: 22px;
      margin-bottom: 0;
      margin-top: 10px;
    }
  }
`;
