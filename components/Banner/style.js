import { css } from "@emotion/core";

export const slider = css`
  padding: 15px 35px;

  .slide {
    height: 280px;
    line-height: 280px;
    text-align: center;
    background: #eaeaec;
    border-radius: 10px;
    color: #d4d4d4;
    font-size: 40px;
    font-weight: 600;
  }

  .slick-dots {
    margin: 0;
  }

  .slick-dots-bottom {
    bottom: 50px;
  }

  .slick-dots li {
    width: 10px !important;
    height: 10px !important;

    &.slick-active button {
      background: #a2a2a2;
      opacity: 1;
    }

    button {
      height: 10px;
      border-radius: 50%;
      background: #a0a0a0;
    }
  }
`;
