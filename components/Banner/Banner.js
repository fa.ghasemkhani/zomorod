// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { Carousel } from "antd";
import { slider } from "./style";

const Banner = () => {
  return (
    <Carousel css={slider}>
      <div>
        <h1 className="slide">ADS</h1>
      </div>
      <div>
        <h1 className="slide">ADS</h1>
      </div>
      <div>
        <h1 className="slide">ADS</h1>
      </div>
      <div>
        <h1 className="slide">ADS</h1>
      </div>
      <div>
        <h1 className="slide">ADS</h1>
      </div>
    </Carousel>
  );
};

export default Banner;
