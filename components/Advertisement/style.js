import { css } from "@emotion/core";

export const ads = css`
  padding: 50px 35px 0 35px;

  .ads {
    max-width: 100%;
    height: auto;
    border-radius: 10px;
  }
`;
