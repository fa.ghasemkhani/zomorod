// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { ads } from "./style";

type PropTypes = {
  data?: Object
};

const Advertisement = ({ data }: PropTypes) => {
  const imageData = data && data?.content?.items?.[0]?.main_pair?.icon;

  return (
    <section css={ads}>
      <img src={imageData?.image_path} alt={imageData?.alt} className="ads" />
    </section>
  );
};

export default Advertisement;
