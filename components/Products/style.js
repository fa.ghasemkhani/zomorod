import { css } from "@emotion/core";

export const products = css`
  .productSectionHeader {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 35px;

    h1 {
      margin: 0;
    }

    .arrow {
      color: #000;

      svg {
        font-size: 34px;
      }
    }
  }

  .row {
    display: flex;
    padding-left: 35px;
  }
`;
