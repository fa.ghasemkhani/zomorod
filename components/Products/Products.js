// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { Row, Col } from "antd";
import ProductCard from "../ProductCard/ProductCard";
import { FiChevronRight } from "react-icons/fi";
import CPLink from "../CPLink/CPLink";
import { products } from "./style";

type PropTypes = {
  title?: string,
  data?: Object,
  link?: string
};

const Products = ({ title, data, link = "/" }: PropTypes) => {
  const renderProducts = () => {
    const getData = data && Object?.values(data?.content?.items)?.splice(0, 2);
    return getData?.map((item: Object) => <ProductCard data={item} key={item?.product_id} />);
  };

  return (
    <div css={products}>
      <div className="productSectionHeader">
        <h1>{title}</h1>
        <CPLink className="arrow" href={link} icon={<FiChevronRight />} />
      </div>
      <div className="row">{renderProducts()}</div>
    </div>
  );
};

export default Products;
