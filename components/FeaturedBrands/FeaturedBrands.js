// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { Row, Col } from "antd";
import { featuredBrands } from "./style";

const data = [
  {
    id: 1,
    src: "/images/brands.jpg"
  },
  {
    id: 2,
    src: "/images/brands.jpg"
  },
  {
    id: 3,
    src: "/images/brands.jpg"
  },
  {
    id: 4,
    src: "/images/brands.jpg"
  },
  {
    id: 5,
    src: "/images/brands.jpg"
  },
  {
    id: 6,
    src: "/images/brands.jpg"
  },
  {
    id: 7,
    src: "/images/brands.jpg"
  },
  {
    id: 8,
    src: "/images/brands.jpg"
  }
];

const FeaturedBrands = () => {
  return (
    <section css={featuredBrands}>
      <Row gutter={[15, 15]}>
        {data.map((item: Object) => (
          <Col span={6} key={item?.id}>
            <img src={item.src} alt="brand" className="brand" />
          </Col>
        ))}
      </Row>
    </section>
  );
};

export default FeaturedBrands;
