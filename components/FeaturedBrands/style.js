import { css } from "@emotion/core";

export const featuredBrands = css`
  padding: 45px 35px 35px 35px;
  background: #e9ebed;
  margin: 80px 0 0 0;

  .brand {
    border-radius: 10px;
    max-width: 100%;
    height: auto;
  }
`;
