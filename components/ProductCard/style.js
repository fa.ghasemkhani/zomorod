import { css } from "@emotion/core";

export const card = css`
  border-radius: 10px;
  width: 60%;
  margin-right: 30px;

  .ant-card-cover img {
    border-radius: 10px 10px 0 0;
    border: 1px solid #f0f0f0;
  }
`;
