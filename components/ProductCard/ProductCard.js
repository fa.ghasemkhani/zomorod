// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { Card } from "antd";
import { card } from "./style";

const { Meta } = Card;

type PropTypes = {
  data?: Object
};

const ProductCard = ({ data }: PropTypes) => {
  const { alt, image_path } = Object.values(data?.main_pair?.icons)?.[1] || {};

  return (
    <Card css={card} hoverable cover={<img alt={alt} src={image_path} />}>
      <Meta title={data?.product} description={data?.price} />
    </Card>
  );
};

export default ProductCard;
