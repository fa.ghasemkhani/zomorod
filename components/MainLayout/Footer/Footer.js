// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { footer } from "./style";
import CPLink from "../../CPLink/CPLink";
import { FaTwitter, FaInstagram, FaLinkedinIn, FaTelegramPlane } from "react-icons/fa";
import { Input, Divider } from "antd";

const { Search } = Input;

const LayoutFooter = () => {
  return (
    <footer css={footer}>
      <div className="help">
        <h4>How can we help?</h4>
        <p>Contact us anytime</p>
        <div className="info">
          <span className="part">
            <p>SEND US A MESSAGE</p>
            <p>contact@zomorod.com</p>
          </span>
          <Divider type="vertical" />
          <span className="part">
            <p>CALL US</p>
            <p>+968 2447 6262</p>
          </span>
        </div>
      </div>
      <div className="links">
        <div className="col">
          <h4>
            ZOMOROD <b>info</b>
          </h4>
          <ul className="mainFooterMenu">
            <li>
              <CPLink text="About Us" href="/" />
            </li>
            <li>
              <CPLink text="Contact Us" href="/" />
            </li>
            <li>
              <CPLink text="Job Offers" href="/" />
            </li>
          </ul>
        </div>
        <div className="col">
          <h4>
            ZOMOROD <b>support</b>
          </h4>
          <ul className="mainFooterMenu">
            <li>
              <CPLink text="How to Use" href="/" />
            </li>
            <li>
              <CPLink text="FAQ" href="/" />
            </li>
            <li>
              <CPLink text="Blog / Magazine" href="/" />
            </li>
          </ul>
        </div>
      </div>
      <div className="emailUs">
        <Divider />
        <b className="text">Say s.th like: Knows about discounts and our new products! </b>
        <Search
          className="input"
          placeholder="Your email address"
          enterButton="SEND"
          size="large"
        />
        <Divider />
      </div>
      <div className="socials">
        <h1 className="logo">LOGOTYPE</h1>
        <b>A quick and easy online shop to buy products you need.</b>
        <ul className="socialMenu">
          <li>
            <CPLink className="link" icon={<FaTwitter />} href="/" />
          </li>
          <li>
            <CPLink className="link" icon={<FaInstagram />} href="/" />
          </li>
          <li>
            <CPLink className="link" icon={<FaLinkedinIn />} href="/" />
          </li>
          <li>
            <CPLink className="link" icon={<FaTelegramPlane />} href="/" />
          </li>
        </ul>
      </div>
      <div className="copyWrite">
        <p>© 2020 Zomorod Ecommerce LLC. All rights reserved.</p>
      </div>
    </footer>
  );
};

export default LayoutFooter;
