import { css } from "@emotion/core";

export const footer = css`
  margin-top: 85px;

  .help {
    background: #c7c7c7;
    padding: 30px;
    text-align: center;
    color: #fff;

    h4,
    p {
      color: #fff;
      margin: 0;
      line-height: 26px;
    }

    h4 {
      font-weight: 600;
      font-size: 24px;
    }

    p {
      font-size: 22px;
    }

    .info {
      background: #fff;
      border-radius: 10px;
      margin: 20px auto 0 auto;
      display: flex;
      align-items: center;
      justify-content: space-around;
      height: 100px;
      max-width: 560px;

      .ant-divider-vertical {
        height: 2.9em;
        border-left: 2px solid #d2d2d2;
      }

      .part {
        text-align: left;

        p {
          font-size: 16px;
          color: #a9a9a9;

          &:first-child {
            color: #dadada;
          }
        }
      }
    }
  }

  .links {
    display: flex;
    justify-content: space-between;
    background: #b1b1b1;
    padding: 45px 35px 20px 35px;

    .col {
      h4 {
        font-size: 20px;
        font-weight: 600;
        color: #fff;

        b {
          font-size: 16px;
        }
      }
    }

    .mainFooterMenu {
      list-style: none;
      margin: 0;
      padding: 0;

      li a {
        color: #fff;
        font-size: 20px;
      }
    }
  }

  .emailUs {
    background: #b1b1b1;
    padding: 0 35px;

    .text {
      display: block;
      text-align: center;
      color: #fff;
      margin-top: 20px;
      font-size: 20px;
    }

    .ant-divider {
      margin: 0;
      border-top: 3px solid #fff;
    }

    .input {
      margin: 25px 0;
    }

    .input,
    .ant-input-group,
    input {
      height: 80px;
      background: #eaeaea;
      border: none;
      border-radius: 10px;
    }

    .ant-input-group-addon {
      background: #eaeaea;
      padding-right: 10px;
      border-radius: 10px;
    }

    input {
      border-radius: 10px 0 0 10px;
    }

    .ant-btn-primary {
      color: #000;
      background: #fff;
      border: none;
      box-shadow: none;
      height: 60px;
      border-radius: 10px !important;
      width: 180px;
    }
  }
  .socials {
    background: #b1b1b1;
    padding: 65px 0 35px 0;
    text-align: center;

    .logo {
      margin: 0;
      font-size: 24px;
      color: orange;
      font-weight: 600;
    }

    b {
      display: block;
      color: #fff;
      text-align: center;
      font-size: 20px;
      padding: 45px 0px;
    }

    .socialMenu {
      margin: 0;
      display: flex;
      justify-content: center;
      list-style: none;

      .link {
        margin: 10px;
        display: block;
        background: white;
        color: gray;
        border-radius: 50%;
        box-shadow: 0 0 5px #ddd;
        width: 60px;
        height: 60px;
        line-height: 70px;
        text-align: center;
        font-size: 26px;
      }
    }
  }
  .copyWrite {
    height: 100px;
    color: #fff;
    text-align: center;
    background: #969696;
    line-height: 100px;
    font-size: 24px;

    p {
      margin: 0;
    }
  }
`;
