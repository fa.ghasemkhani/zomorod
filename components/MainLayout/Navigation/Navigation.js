// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React, { useState } from "react";
import { navigation } from "./style";
import MobileMenu from "../MobileMenu/MobileMenu";
import { Button, Input } from "antd";
import { FiShoppingBag, FiMenu, FiChevronRight } from "react-icons/fi";
import { BsFillQuestionCircleFill } from "react-icons/bs";

const { Search } = Input;

const Navigation = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false);

  // Handle mobile menu visibility
  const showMenu = () => {
    setShowMobileMenu(!showMobileMenu);
  };

  return (
    <header>
      <nav css={navigation}>
        <span className="firstBanner">Best Quality, Best Price</span>
        <div className="mainNav">
          <span className="leftItems">
            <Button className="collapse" icon={<FiMenu />} onClick={showMenu} />
            <h1 className="logo">LOGOTYPE</h1>
          </span>
          <span className="rightItems">
            <Button className="signUp">Sign-Up</Button>
            <BsFillQuestionCircleFill className="help" />
          </span>
        </div>
        <div className="searchBasket">
          <FiShoppingBag className="basket" />
          <Search />
        </div>
        <div className="location">
          <span className="currentLocation">
            <img src="/images/flag.png" alt="" />
            <p className="defaultLocation">Default location</p>
          </span>
          <FiChevronRight className="arrow" />
        </div>
      </nav>
      <MobileMenu visible={showMobileMenu} onClose={showMenu} />
    </header>
  );
};

export default Navigation;
