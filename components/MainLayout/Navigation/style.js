import { css } from "@emotion/core";

export const navigation = css`
  .firstBanner {
    display: block;
    padding: 14px 0;
    text-align: center;
    font-size: 24px;
    background: #f3f4f4;
  }

  .mainNav,
  .searchBasket,
  .location {
    display: flex;
    justify-content: space-between;
    padding: 0 35px;
    background: #f9f9fb;
  }

  .mainNav {
    padding-top: 25px;
    padding-bottom: 25px;
    align-items: center;

    .rightItems,
    .leftItems {
      display: flex;
      align-items: center;
    }

    .leftItems {
      .collapse {
        width: 28px;
        height: 28px;
        padding: 0;
        background: transparent;
        border: none;
        margin-right: 50px;

        svg {
          font-size: 30px;
        }
      }

      .logo {
        margin: 0;
        font-size: 24px;
        color: orange;
        font-weight: 600;
      }
    }

    .rightItems {
      .signUp {
        height: 60px;
        width: 190px;
        border-radius: 10px;
        font-size: 24px;
        border: 2px solid #fff;
        background: #eaeaec;
      }

      .help {
        color: #3a3c42;
        font-size: 34px;
        display: block;
        margin-left: 32px;
      }
    }
  }

  .searchBasket {
    display: flex;
    align-items: center;
    padding-bottom: 20px;
    justify-content: space-between;

    .basket {
      font-size: 45px;
      margin-right: 40px;
    }

    .ant-input,
    .ant-input-search-button,
    .ant-input-group-addon,
    .ant-input-group {
      height: 78px;
    }

    .ant-input,
    .ant-input-search-button {
      border: 2px solid #d1d2d2;
      border-radius: 10px;
    }

    .ant-input {
      border-right: none !important;
    }

    .ant-input-search-button {
      width: 64px;
      border-left: none !important;

      svg {
        font-size: 35px;
        color: #000;
      }
    }

    .ant-input-search > .ant-input-group > .ant-input-group-addon .ant-input-search-button {
      border-radius: 0 10px 10px 0;
    }

    .ant-input-group > .ant-input:first-child,
    .ant-input-group-addon:first-child {
      border-top-right-radius: 0 !important;
      border-bottom-right-radius: 0 !important;
    }
  }

  .location {
    border-top: 2px solid #f0f0f0;
    border-bottom: 2px solid #f0f0f0;
    padding: 20px 35px !important;

    .currentLocation {
      display: flex;
      align-items: center;

      img {
        height: 24px;
      }

      .defaultLocation {
        display: inline;
        font-size: 24px;
        margin: 0;
        margin-left: 24px;
      }
    }

    .arrow {
      height: 50px;
      width: 50px;
      color: #d1d3d6;
    }
  }
`;
