// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React, { type Node } from "react";
import { Layout } from "antd";
import Navigation from "./Navigation/Navigation";
import Footer from "./Footer/Footer";
import { mainLayout } from "./style";
const { Content } = Layout;

type PropTypes = {
  children: Node
};

const MainLayout = ({ children }: PropTypes) => {
  return (
    <div css={mainLayout}>
      <Navigation />
      <Content>{children}</Content>
      <Footer />
    </div>
  );
};

export default MainLayout;
