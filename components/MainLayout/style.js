import { css } from "@emotion/core";

export const mainLayout = css`
  max-width: 750px;
  margin: 0 auto;
  background: #ffffff;
  overflow: hidden;
`;
