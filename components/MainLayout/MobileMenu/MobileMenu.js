// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { Drawer } from "antd";
import { drawer } from "./style";

type PropTypes = {
  visible: boolean,
  onClose: Function
};

const mobileMenu = ({ visible, onClose }: PropTypes) => {
  return (
    <Drawer css={drawer} placement="left" onClose={onClose} visible={visible} title="Mobile menu">
      <p>Links ...</p>
      <p>Links ...</p>
      <p>Links ...</p>
    </Drawer>
  );
};

export default mobileMenu;
