import { css } from "@emotion/core";

export const categories = css`
  padding: 50px 28px;

  .slick-dots li button:before {
    font-size: 30px;
    color: #a2a2a2;
  }

  .slideWrapper {
    text-align: center;

    img {
      border-radius: 10px;
      width: 100%;
      padding: 6px;
    }
  }
`;
