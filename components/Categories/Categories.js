// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from "react";
import { categories } from "./style";
import Slider from "react-slick";

// Slider mock data
const data = [
  { id: 1, src: "/images/categories.jpg", category: "category 1" },
  { id: 2, src: "/images/categories.jpg", category: "category 2" },
  { id: 3, src: "/images/categories.jpg", category: "category 3" },
  { id: 4, src: "/images/categories.jpg", category: "category 4" },
  { id: 5, src: "/images/categories.jpg", category: "category 5" },
  { id: 6, src: "/images/categories.jpg", category: "category 6" },
  { id: 7, src: "/images/categories.jpg", category: "category 7" },
  { id: 8, src: "/images/categories.jpg", category: "category 8" }
];

const Categories = () => {
  // Slider props
  const settings = {
    dots: true,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 4
  };

  return (
    <section css={categories}>
      <Slider {...settings}>
        {data.map((item: Object) => (
          <span className="slideWrapper" key={item.id}>
            <img src={item.src} alt="" />
            <p>{item.category}</p>
          </span>
        ))}
      </Slider>
    </section>
  );
};

export default Categories;
