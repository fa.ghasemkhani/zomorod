// @flow
import React, { type Node } from "react";
import Link from "next/link";

type PropTypes = {
  href?: string,
  className?: string,
  text?: string,
  icon?: Node,
  onClick?: Function
};

const CPLink = ({ href, className, text, icon, onClick }: PropTypes) => {
  return (
    <Link href={href} onClick={onClick}>
      <a className={className}>
        {icon && icon} {text && text}
      </a>
    </Link>
  );
};

export default CPLink;
