// @flow
/** @jsx jsx */
import { jsx } from "@emotion/core";
import React, { useEffect, useState } from "react";
import Banner from "../components/Banner/Banner";
import Features from "../components/Features/Features";
import Products from "../components/Products/Products";
import Advertisement from "../components/Advertisement/Advertisement";
import FeaturedBrands from "../components/FeaturedBrands/FeaturedBrands";
import Categories from "../components/Categories/Categories";

type PropTypes = {
  data?: Object
};

const Home = ({ data }: PropTypes) => {
  console.log(data);
  const [bannersData, setBannersData] = useState(null);
  const [productsData, setProductsData] = useState(null);

  // Set banners and products data
  useEffect(() => {
    if (data) {
      const [banners, products] =
        data &&
        Object?.values(data)?.filter(
          (items: Object) => items?.name === "Best Sellers SPA" || items?.name === "Main banners"
        );

      setBannersData(banners);
      setProductsData(products);
    }
  }, [data]); // eslint-disable-line

  return (
    <>
      <Banner />
      <Features />
      <Products title="Flash Sale" data={productsData} />
      <Advertisement data={bannersData} />
      <Products title="New in" data={productsData} />
      <Categories />
      <Products title="Best Seller" data={productsData} />
      <Advertisement data={bannersData} />
      <Products title="Extra Saving/Daily Drops" data={productsData} />
      <FeaturedBrands />
      <Advertisement data={bannersData} />
      <Products title="Most Viewed" data={productsData} />
      <Products title="Most favorite" data={productsData} />
    </>
  );
};

export default Home;
