// @flow
import "antd/dist/antd.css";
import React, { useEffect, useState } from "react";
import { Spin } from "antd";
import MainLayout from "../components/MainLayout/MainLayout";
import { request } from "../store/request";
import { api } from "../resources/api";

type PropTypes = {
  Component: () => void
};

const MyApp = ({ Component }: PropTypes) => {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);

  // Call api to set data and loading
  useEffect(() => {
    let reCallAction = false;

    if (!reCallAction) {
      (async () => {
        setLoading(true);
        const response = await request?.get(api);

        if (response?.data) {
          setData(response?.data);
        }
        setLoading(false);
      })();
    }

    return () => {
      reCallAction = true;
    };
  }, []); // eslint-disable-line

  return (
    <Spin spinning={loading} tip="Loading...">
      <MainLayout>
        <Component data={data} />
      </MainLayout>
    </Spin>
  );
};

export default MyApp;
