const { create } = require("apisauce");
const baseURL = "https://zomorod.com";

const request = create({
  baseURL,
  headers: {
    "Content-Type": "application/json"
  }
});

module.exports = {
  request,
  baseURL
};
